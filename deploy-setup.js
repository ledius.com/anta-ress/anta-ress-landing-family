const config = {
    app: {
        name: 'family-law',
        domains: {
            production: 'familylaw.anta-ress.ru',
            development: 'familylaw-dev.anta-ress.ru'
        }
    },
    kube: {
        namespace: 'anta-ress'
    },
    registry: {
        path: 'registry.gitlab.com/ledius.com/anta-ress/anta-ress-landing-family:main'
    },
    node: {
        version: '16',
    }
};




(async () => {
    const fs = require('fs/promises');
    await fs.rm('.kube', {
        recursive: true,
        force: true,
    });
    await fs.mkdir(`.kube`);
    await fs.writeFile(`Dockerfile`, Buffer.from(Dockerfile()));
    await fs.writeFile(`.gitlab-ci.yml`, Buffer.from(GitlabCIFile()));
    await fs.writeFile(`.kube/${config.app.name}.deployment.yaml`, Buffer.from(DeploymentFile()));
    await fs.writeFile(`.kube/${config.app.name}.ingress.yaml`, Buffer.from(IngressFile()));
    await fs.writeFile(`.kube/${config.app.name}.service.yaml`, Buffer.from(ServiceFile()));
})();


function Dockerfile() {
    return `FROM node:${config.node.version} as build-stage
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build

FROM nginx:latest as production
COPY --from=build-stage /app /usr/share/nginx/html
`
}

function DeploymentFile () {
    return `apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${config.app.name}
  labels:
    app: ${config.app.name}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ${config.app.name}
  template:
    metadata:
      labels:
        app: ${config.app.name}
    spec:
      containers:
        - name: ${config.app.name}
          image: ${config.registry.path}
          imagePullPolicy: "Always"
          ports:
            - containerPort: 80
      imagePullSecrets:
        - name: registry.gitlab
`
}


function ServiceFile() {
    return `apiVersion: v1
kind: Service
metadata:
  name: ${config.app.name}
spec:
  selector:
    app: ${config.app.name}
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
`;
}

function IngressFile(){
    return `apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ${config.app.name}
  labels:
    name: ${config.app.name}
spec:
  ingressClassName: nginx
  rules:
  - host: ${config.app.domains.production}
    http:
      paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: ${config.app.name}
              port:
                number: 80
  - host: ${config.app.domains.development}
    http:
      paths:
        - path: /
          pathType: Prefix
          backend:
              service:
                name: ${config.app.name}
                port:
                  number: 80
`;
}

function GitlabCIFile() {
    return `stages:
  - build
  - deploy

before_script:
  - echo $DO_K8S_CONFIG > .kube/config

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""


Build:
  stage: build
  image: docker
  services:
    - name: docker:dind
      alias: docker
      command: [ "--tls=false" ]
  before_script:
    - docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
  script:
    - docker build -t \${CI_REGISTRY_IMAGE}:\${CI_COMMIT_REF_NAME} -f ./Dockerfile .
    - docker push \${CI_REGISTRY_IMAGE}:\${CI_COMMIT_REF_NAME}
  only:
    - tags
    - main

Deploy production:
  stage: deploy
  image: alpine:3.7
  before_script:
    - apk add --no-cache curl
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
  environment:
    name: ${config.kube.namespace}
    kubernetes:
      namespace: ${config.kube.namespace}
  script:
    - echo $KUBECONFIG
    - export KUBECONFIG=$KUBECONFIG
    - ./kubectl apply -f .kube
    - ./kubectl rollout restart deployment ${config.app.name}
`
}
