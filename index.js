// === CONFIG ===

const apiUrl = '';
const instUrl = 'https://www.instagram.com/55advokat55/';
const vkUrl = 'https://vk.com/jur55';
const viberPhone = '79087955703';

// === DOM ELEMENTS ===

const dots = document.querySelectorAll('.dot');
const dotsArrows = document.querySelectorAll('.dots button');
const inputs = document.querySelectorAll('input');
const slider = document.querySelector('.intro-slider');
const sliderElements = document.querySelectorAll('.intro-bg');
const sliderArrows = document.querySelectorAll('.slider-btn');
const burgerBtn = document.querySelector('.burger-btn');
const burgerMenu = document.querySelector('.burger-menu');
const burgerMenuElements = document.querySelectorAll('.burger-menu button');
const form = document.querySelector('.intro form');
const inst = document.querySelectorAll('.media-inst');
const vk = document.querySelectorAll('.media-vk');
const viber = document.querySelectorAll('.media-viber');

// === VARIABLES ===

let timer;
let posX1, posX2;
let intervalStopped = false;

// === METHODS ===

const dotClickHandler = (e, num = 1) => {
    clearInterval(timer);
    intervalStopped = true;
    let index;
    if (e == null) {
        index = 1;
    } else index = e.target.id.split('-')[1];
    if (index == 1) {
        dots.forEach(e =>
            e.classList.contains('active') ? e.classList.remove('active') : ''
        );
        dots[index - 1].classList.add('active');
        slider.style = 'transform: translateX(33.3%)';
    } else {
        dots.forEach(e =>
            e.classList.contains('active') ? e.classList.remove('active') : ''
        );
        slider.style = `transform: translateX(${
            (-100 / 3) * (e.target.id.split('-')[1] - 2)
        }%)`;
        dots[index - 1].classList.add('active');
    }
};

const sliderStopper = () => {
    if (intervalStopped) {
        clearInterval(timer);
        intervalStopped = true;
    } else {
        intervalSetter();
    }
};

const intervalSetter = () => {
    timer = setInterval(() => sliderNext(), 7500);
    intervalStopped = false;
};

const sliderNext = () => {
    let updated = false;
    dots.forEach((e, i) => {
        if (e.classList.contains('active') && !updated) {
            if (dots[i + 1]) {
                dots[i].classList.remove('active');
                dots[i + 1].classList.add('active');
                slider.style = `transform: translateX(${(-100 / 3) * i}%)`;
                updated = true;
            } else {
                dots[i].classList.remove('active');
                dots[0].classList.add('active');
                slider.style = 'transform: translateX(33.3%)';
                updated = true;
            }
        }
    });
};

const sliderPrev = () => {
    let updated = false;
    dots.forEach((e, i) => {
        if (e.classList.contains('active') && !updated) {
            if (dots[i - 1]) {
                dots[i].classList.remove('active');
                slider.style = `transform: translateX(${
                    33.3 * (i == 2 ? 0 : 1)
                }%)`;
                dots[i - 1].classList.add('active');
                updated = true;
            } else {
                dots[i].classList.remove('active');
                slider.style = `transform: translateX(-33.3%)`;
                dots[dots.length - 1].classList.add('active');
                updated = true;
            }
        }
    });
};

const burgerClickHandler = () => {
    if (burgerMenu.classList.contains('active')) {
        burgerClose();
    } else {
        burgerOpen();
    }
};

const burgerOpen = () => {
    document.documentElement.style = 'overflow-y: hidden';
    burgerMenu.style = 'z-index: 2';
    burgerMenu.classList.add('active');
    burgerBtn.classList.add('isOpen');
};

const burgerClose = () => {
    burgerMenu.classList.remove('active');
    document.documentElement.style = 'overflow-y: unset';
    setTimeout(() => (burgerMenu.style = 'z-index: -1'), 200);
    burgerBtn.classList.remove('isOpen');
};

const orderClickHandler = () => {
    scrollYSetter(0, 0);
    dotClickHandler(null, 1);
    burgerClose();
    setTimeout(() => form.children['name'].focus(), 1000);
};

const scrollYSetter = (id = 0, px = 0) => {
    if (id == 0) window.scroll({ top: 0, behavior: 'smooth' });
    else {
        window.scroll({
            top: document.querySelector(`#${id}`).offsetTop - px,
            behavior: 'smooth',
        });
    }
};

const linksUrlSetter = () => {
    inst.forEach(e => e.setAttribute('href', `${instUrl}`));
    vk.forEach(e => e.setAttribute('href', `${vkUrl}`));
    viber.forEach(e =>
        e.setAttribute('href', `viber://chat?number=%2B${viberPhone}`)
    );
};

const sliderImgsSetter = (width = 100) => {
    if (width >= 644 && width < 1046) {
        if (sliderElements[0].getAttribute('data-width') !== 'M') {
            sliderElements[0].style =
                'background-image: url("./imgs/intro1M.png")';
            sliderElements[1].style =
                'background-image: url("./imgs/intro2M.png")';
            sliderElements[2].style =
                'background-image: url("./imgs/intro3M.png")';
        }
        sliderElements[0].setAttribute('data-width', 'M');
    } else if (width >= 1047 && width < 1600) {
        if (sliderElements[0].getAttribute('data-width') !== 'L') {
            sliderElements[0].style =
                'background-image: url("./imgs/intro1L.png")';
            sliderElements[1].style =
                'background-image: url("./imgs/intro2L.png")';
            sliderElements[2].style =
                'background-image: url("./imgs/intro3L.png")';
        }
        sliderElements[0].setAttribute('data-width', 'L');
    } else if (width >= 1600) {
        if (sliderElements[0].getAttribute('data-width') !== 'XL') {
            sliderElements[0].style =
                'background-image: url("./imgs/intro1XL.png")';
            sliderElements[1].style =
                'background-image: url("./imgs/intro2XL.png")';
            sliderElements[2].style =
                'background-image: url("./imgs/intro3XL.png")';
        }
        sliderElements[0].setAttribute('data-width', 'XL');
    } else if (width < 644) {
        if (sliderElements[0].getAttribute('data-width') !== 'S') {
            sliderElements[0].style =
                'background-image: url("./imgs/intro1S.png")';
            sliderElements[1].style =
                'background-image: url("./imgs/intro2S.png")';
            sliderElements[2].style =
                'background-image: url("./imgs/intro3S.png")';
        }
        sliderElements[0].setAttribute('data-width', 'S');
    }
};

// TODO task for Artyom
const sendBid = async e => {
    e.preventDefault();
    const res = await fetch(apiUrl, {
        method: 'POST',
    })
        .then(res => console.log(res))
        .catch(err => console.log(err.response));
};

// === INIT METHODS ===

intervalSetter();
linksUrlSetter();
sliderImgsSetter(window.innerWidth);

// === LISTENERS ===

// when form submit send request
form.addEventListener('submit', e => sendBid(e));

// click on burger menu button
burgerMenu.addEventListener('click', burgerClickHandler);

// click on burger menu elements doesn't close burger menu
burgerMenuElements.forEach(e =>
    e.addEventListener('click', e => {
        e.stopPropagation();
    })
);

// click on arrow element turn off swiping
sliderArrows.forEach(e =>
    e.addEventListener('click', e => {
        e.stopPropagation();
        intervalStopped = true;
        sliderStopper();
    })
);

dotsArrows.forEach(e =>
    e.addEventListener('click', e => {
        e.stopPropagation();
        intervalStopped = true;
        sliderStopper();
    })
);

// click on doesn't call swiping to selected slider part
dots.forEach(e => e.addEventListener('click', e => dotClickHandler(e)));

// click on slider block call to stop swiping
sliderElements.forEach(e =>
    e.addEventListener('click', () => {
        intervalStopped = !intervalStopped;
        sliderStopper();
    })
);

// click on form doesn't call start\stop swiping
form.addEventListener('click', e => e.stopPropagation());

// if text typing swiping will stop
inputs.forEach(e =>
    e.addEventListener('input', () => {
        intervalStopped = true;
        sliderStopper();
    })
);

// detect swiping
sliderElements.forEach(e => {
    e.addEventListener(
        'touchstart',
        e => {
            intervalStopped = true;
            sliderStopper();
            posX1 = e.changedTouches[0].screenX;
        },
        { passive: true }
    );
});

// detect when swipe is ended
sliderElements.forEach(e => {
    e.addEventListener(
        'touchend',
        e => {
            posX2 = e.changedTouches[0].screenX;
            if (Math.abs(posX1 - posX2) > 100) {
                if (posX1 - posX2 < 0) {
                    sliderPrev();
                } else {
                    sliderNext();
                }
            }
        },
        { passive: true }
    );
});

// close burger when resize
window.onresize = () => {
    if (window.innerWidth > 643) {
        burgerClose();
    }
    sliderImgsSetter(window.innerWidth);
};

// return swiping when user doesn't interact with slider
let updated = false;
window.onscroll = () => {
    if (window.scrollY && intervalStopped && !updated) {
        updated = true;
        intervalSetter();
    }
};
